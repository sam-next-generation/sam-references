package com.syn.sam.api.service;

import com.syn.sam.api.repository.RefBiCheckRepository;
import com.syn.sam.model.RefBicheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.BusinessException;

import java.util.List;

/**
 * Created by rachmanareef on 1/11/18.
 */
@Service
public class ReferencesService {
    @Autowired
    RefBiCheckRepository refBiCheckRepository;
    public List<RefBicheck> getAllBiCheck() throws BusinessException{
        List<RefBicheck> bichecks = refBiCheckRepository.findAll();
        if(bichecks.isEmpty()){
            throw new BusinessException("Not Found");
        }
        return bichecks;
    }
}
