package com.syn.sam.api.repository;

import com.syn.sam.model.RefBicheck;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by rachmanareef on 1/10/18.
 */

public interface RefBiCheckRepository extends JpaRepository<RefBicheck, Short> {
}
