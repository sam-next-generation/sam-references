package com.syn.sam.api.endpoint;

import com.syn.sam.api.repository.RefBiCheckRepository;
import com.syn.sam.api.service.ReferencesService;
import com.syn.sam.model.RefBicheck;
import com.syn.sam.response.ResponseMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import util.BusinessException;

import java.util.List;

/**
 * Created by rachmanareef on 1/10/18.
 */
@RestController
public class ReferencesEndpoint {
    @Autowired
    ReferencesService referencesService;

    @RequestMapping("/bicheck")
    public ResponseMsg getBiCheck() throws BusinessException{
        List<RefBicheck> refBichecks = referencesService.getAllBiCheck();
        ResponseMsg<List<RefBicheck>> msg = new ResponseMsg<>();
        msg.setRc("0");
        msg.setRm("Success");
        msg.setData(refBichecks);
        return msg;
    }
}
