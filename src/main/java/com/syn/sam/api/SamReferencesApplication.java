package com.syn.sam.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "com.syn.sam.model")
@EnableJpaRepositories(basePackages = "com.syn.sam.api.repository")
public class SamReferencesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamReferencesApplication.class, args);
	}
}
